# DISCLAIMER

### Non mi assumo nessuna responsabilità per ciascun utilizzo di questo programma

Il token di autenticazione e i servizi restful invocati mediante esso, possono essere utilizzati solo dall'applicazione "DidUP - Famiglia" della Argo Software SRL per l’erogazione dei propri servizi o da fornitori saas e relative applicazioni appositamente preautorizzate, in conformità alla vigente normativa in maniera di protezione dei dati personali ed alle misure richieste dall’AgID per gli applicativi SaaS delle PA.

Questo programma non è stato autorizzato per l'erogazioni dei sopracitati servizi ed è stato sviluppato solo a livello teorico

### Sviluppato per linux

# Installazione

`pip3 install --user -r requirements.txt`

`mkdir dati`

# Esecuzione

## Per utilizzarlo via linea di comando

`cd cli-ui`

`python3 client.py`

## Per utilizzarlo via web

`cd web-ui`

`python3 web-ui.py`

## Per vedere le credenziali

`cd tools`

`python3 view-cred.py`

### Questo programma è stato reso possibile da: [hearot/ArgoScuolaNext-Python](https://github.com/hearot/ArgoScuolaNext-Python)
